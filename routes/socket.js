var express = require("express");
var router = express.Router();
const Socket = require("../models/socker_collection");
/* GET home page. */
router.get("/socket_price", async (req, res, next) => {
  try {
    const sockets = await Socket.find();
    const summarysockets = []
    if(sockets){
        sockets[0] && sockets.forEach((data,i)=>{
            summarysockets.push([Date.parse(data.createdate),parseFloat(data.portamount)])
        })
    }
    res.json(summarysockets);
  } catch (error) {
    res.json({ message: error.message });
  }
});

module.exports = router;
