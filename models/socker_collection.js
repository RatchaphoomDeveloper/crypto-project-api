const mongoose = require('mongoose')

const socketCollectionSchrema = mongoose.Schema({
    "_id" : {type : String},
    portamount:{
        type: String,
        required:true,
    },
    createdate:{
        type:Date,
        default:Date.now
    },
    updatedate:{
        type:Date,
        default:Date.now
    }
})

module.exports = mongoose.model('SOCKET_COLLECTION',socketCollectionSchrema,'SOCKET_COLLECTION')